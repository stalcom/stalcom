package com.stalcom.stdbk.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.stalcom.stdbk.dao.structure.UserDAOStructure;
import com.stalcom.stdbk.dto.User;
import com.stalcom.stdbk.jdbc.JDBCConnection;

public class UserDAO implements UserDAOStructure {
	private static UserDAO instance = new UserDAO();

	private UserDAO() {}
	public static UserDAO getInstance() {
		return instance;
	}
	
	@Override
	public Map<String,User> getAllUsers() throws ClassNotFoundException, SQLException {
		Map<String,User> users = new LinkedHashMap<String,User>();
		
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		try {
			con = JDBCConnection.getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery("SELECT email,password FROM user");
			
			while(rs.next()) {
				users.put(rs.getString("email"), new User(rs.getString("email"), rs.getString("password")));
			}
			
			return users;
			
		} catch (ClassNotFoundException e) {
			throw new ClassNotFoundException();
		} catch (SQLException e) {
			throw new SQLException();
		} finally {
			try {
				if(rs != null) rs.close();
				if(stmt != null) stmt.close();
				if(con != null) con.close();
			} catch (SQLException e) {
				throw new SQLException();
			}
		}
	}

	@Override
	public int[] updateAllUsers(List<User> users) throws ClassNotFoundException, SQLException {
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			con = JDBCConnection.getConnection();
			pstmt = con.prepareStatement("INSERT INTO user (email,password) VALUES (?,?)");
			
			con.setAutoCommit(false);
			
			for(User user:users) {
				pstmt.setString(1, user.getEmail());
				pstmt.setString(2, user.getPassword());
				
				pstmt.addBatch();
			}
			
			int[] totalUpdates = pstmt.executeBatch();
			
			if(totalUpdates.length == users.size()) {
				con.commit();
			}
			
			/* cleaning the temporary location */
			users.removeAll(users);
			
			return totalUpdates;
			
		} catch (ClassNotFoundException e) {
			throw new ClassNotFoundException();
		} catch (SQLException e) {
			throw new SQLException();
		} finally {
			try {
				if(pstmt != null) pstmt.close();
				if(con != null) con.close();
			} catch (SQLException e) {
				throw new SQLException();
			}
		}
	}
	
}
