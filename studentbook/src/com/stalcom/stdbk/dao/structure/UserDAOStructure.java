package com.stalcom.stdbk.dao.structure;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.stalcom.stdbk.dto.User;

public interface UserDAOStructure {
	public Map<String,User> getAllUsers() throws ClassNotFoundException, SQLException;
	public int[] updateAllUsers(List<User> users) throws ClassNotFoundException, SQLException;
}
