package com.stalcom.stdbk;

import static com.stalcom.stdbk.util.ContextUtility.getSublist;

import java.sql.SQLException;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.stalcom.stdbk.dao.UserDAO;
import com.stalcom.stdbk.dto.User;

public class StudentbookContextListener implements ServletContextListener {
	private ServletContext context;
	
	@Override
	public void contextDestroyed(ServletContextEvent event) {
		context = event.getServletContext();
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		context = event.getServletContext();
		
		try {
			context.setAttribute("users", UserDAO.getInstance().getAllUsers());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		new Thread(new Runnable(){
			private Map<String,User> users = (Map<String,User>) context.getAttribute("users");
			private int initial = users.size();
			private int total;
			
			@Override
			public void run() {
				while(true) {
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					synchronized(users) {
						total = users.size();
					}
					
					if(total == initial) {
						continue;
					}
					
					try {
						int[] totalUpdates = UserDAO.getInstance().updateAllUsers(getSublist(users, initial, total));
						
						if(totalUpdates.length == (total - initial)) {
							initial = total;
						}
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}).start();
	}

}
