package com.stalcom.stdbk.util;

import java.util.regex.Pattern;

public final class InputValidator {
 
	/*
	  ^						# start of the line
	    [_A-Za-z0-9-\\+]+	#  must start with string in the bracket [ ], must contains one or more (+)
	    (					#   start of group #1
	      \\.[_A-Za-z0-9-]+	#     follow by a dot "." and string in the bracket [ ], must contains one or more (+)
	    )*					#   end of group #1, this group is optional (*)
	    @					#     must contains a "@" symbol
	    [A-Za-z0-9-]+      	#       follow by string in the bracket [ ], must contains one or more (+)
	    (					#         start of group #2 - first level TLD checking
	      \\.[A-Za-z0-9]+  	#           follow by a dot "." and string in the bracket [ ], must contains one or more (+)
	    )*					#         end of group #2, this group is optional (*)
	    (					#         start of group #3 - second level TLD checking
	      \\.[A-Za-z]{2,}  	#           follow by a dot "." and string in the bracket [ ], with minimum length of 2
	    )					#         end of group #3
	  $							#end of the line
	*/
	
	private static final String EMAIL_PATTERN = 
		"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	/*
	  (					# Start of group
        (?=.*\d)		#   must contains one digit from 0-9
        (?=.*[a-z])		#   must contains one lowercase characters
        (?=.*[A-Z])		#   must contains one uppercase characters
        (?=.*[@#$%])	#   must contains one special symbols in the list "@#$%"
        .				#     match anything with previous condition checking
        {6,20}			#        length at least 6 characters and maximum of 20	
      )					# End of group
	*/
	
	private static final String PASSWORD_PATTERN = 
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";
 
 	public static boolean isValidEmail(String email) {
		return Pattern.compile(EMAIL_PATTERN).matcher(email).matches();
	}
	
	public static boolean isValidPassword(String password) {
		return Pattern.compile(PASSWORD_PATTERN).matcher(password).matches();
	}
	
}
