package com.stalcom.stdbk.util;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.stalcom.stdbk.dto.User;

public final class ContextUtility {
	/* Linked-List is used to facilitate ease in insertion and deletion */
	private static List<User> sublist = new LinkedList<User>();
	
	public static List<User> getSublist(Map<String,User> users, int startindex, int endindex) {
		
		/* synchronization at Users-List is so because it's being used by clients to register */
		synchronized(users) {
			sublist.addAll(users.values());
		}
		
		while(sublist.size() != endindex) {
			sublist.remove(endindex);
		}
		
		for(int index=0; index<startindex; index++) {
			sublist.remove(0);
		}
		
		return sublist;
	}
}
