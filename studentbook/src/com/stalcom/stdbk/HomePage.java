package com.stalcom.stdbk;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.stalcom.stdbk.dto.User;

public class HomePage extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		out.print(design());
		out.close();
	}
	private String design() {
		StringBuilder builder = new StringBuilder();
		
		builder.append("<!DOCTYPE html>");
		builder.append("<html lang='en'>");
		  builder.append("<head>");
		    builder.append("<meta charset='utf-8'>");
		    builder.append("<meta http-equiv='X-UA-Compatible' content='IE=edge'>");
		    builder.append("<meta name='viewport' content='width=device-width, initial-scale=1'>");
		    builder.append("<title>studentbook - home page</title>");

		     /* Bootstrap CSS link */
		    builder.append("<link rel='stylesheet' href='http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>");
		  builder.append("</head>");
		  builder.append("<body>");
		    builder.append("<div class='container-fluid'>");
				builder.append("<div class='row'>");
					builder.append("<div class='col-lg-offset-8 col-lg-3' style='padding-top: 10%;'>");
						/* Login form */
						builder.append("<form role='form' action='login.s' method='post'>");
		  					builder.append("<div class='form-group'>");
		    					builder.append("<label class='sr-only' for='email'>Email:</label>");
		    					builder.append("<input type='email' class='form-control input-lg' id='email' name='email' placeholder='email' autocomplete='on' autofocus='autofocus' required='required'>");
		  					builder.append("</div>");
		  					builder.append("<div class='form-group'>");
						    	builder.append("<label class='sr-only' for='password'>Password:</label>");
						    	builder.append("<input type='password' class='form-control input-lg' id='password' name='password' placeholder='password' required='required'>");
							builder.append("</div>");
							builder.append("<div class='alert alert-danger alert-dismissible' role='alert'>");
								builder.append("<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>");
								builder.append("<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>");
								builder.append("<span class='sr-only'>Error:</span>");
								builder.append("Enter a valid email address");
							builder.append("</div>");
							builder.append("<div class='checkbox'>");
						    	builder.append("<label id='remember' class='pull-left'><input type='checkbox' name='remember'><span class='text-muted'> Remember me</span></label>");
						    	builder.append("<label id='help' class='pull-right'><span class='text-primary'>Need help <span class='glyphicon glyphicon-question-sign'></span></span></label>");
							builder.append("</div>");
							builder.append("<button id='login' type='submit' class='btn btn-primary btn-lg btn-block'>");
								builder.append("<span class='glyphicon glyphicon-log-in'></span> Login");
			  				builder.append("</button>");
		     				builder.append("<button id='register' type='button' class='btn btn-warning btn-lg btn-block' data-toggle='modal' data-target='#modal'>");
			     			  builder.append("<span class='glyphicon glyphicon glyphicon-user'></span> New user");
			  	    		builder.append("</button>");
						builder.append("</form>");
					builder.append("</div>");
				builder.append("</div>");
			builder.append("</div>");

		    /* Modal */
		    builder.append("<div class='modal fade' id='modal' tabindex='-1' role='dialog' aria-labelledby='title' aria-hidden='true'>");
		      builder.append("<div class='modal-dialog modal-sm'>");
		        builder.append("<div class='modal-content'>");
		          builder.append("<div class='modal-header'>");
		            builder.append("<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>");
		            builder.append("<h4 class='modal-title' id='title'>New User</h4>");
		          builder.append("</div>");
		          builder.append("<div class='modal-body'>");
		            /* Registration form */
					builder.append("<form role='form' action='registration.s' method='post'>");
		  			  builder.append("<div class='form-group'>");
		    			builder.append("<label class='sr-only' for='email'>Email:</label>");
		    			builder.append("<input type='email' class='form-control input-lg' id='email' name='email' placeholder='email' autocomplete='on' autofocus='autofocus' required='required'>");
		  			  builder.append("</div>");
		  			  builder.append("<div class='form-group'>");
						builder.append("<label class='sr-only' for='password'>Password:</label>");
						builder.append("<input type='password' class='form-control input-lg' id='password' name='password' placeholder='password' required='required'>");
					  builder.append("</div>");
					  builder.append("<div class='alert alert-danger alert-dismissible' role='alert'>");
						builder.append("<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>");
						  builder.append("<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>");
						  builder.append("<span class='sr-only'>Error:</span>");
						  builder.append("Enter a valid email address");
					  builder.append("</div>");
					  builder.append("<div class='checkbox'>");
						builder.append("<label id='agree' class='pull-left'><input type='checkbox' name='agree'>");
						  builder.append("<span class='text-muted'> I agree to the <span class='text-primary'>Terms of Services</span> and <span class='text-primary'>Privacy Policy</span></span>");
						builder.append("</label>");
					  builder.append("</div>");
					  builder.append("<button id='login' type='submit' class='btn btn-success btn-lg btn-block'>");
						builder.append("<span class='glyphicon glyphicon glyphicon-log-in'></span> Register");
			  		  builder.append("</button>");
					builder.append("</form>");
		          builder.append("</div>");
		        builder.append("</div>");
		      builder.append("</div>");
		    builder.append("</div>");

		    /* Bootstrap JQuery link */
		    builder.append("<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'></script>");
		    /* Bootstrap link */
		    builder.append("<script src='http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js'></script>");
		  builder.append("</body>");
		builder.append("</html>");
		
		return builder.toString();
	}
}
