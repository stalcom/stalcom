package com.stalcom.stdbk.reg.page;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RegistrationPage extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		out.print(design());
		out.close();
	}
	private String design() {
		StringBuilder builder = new StringBuilder();
		
		builder.append("<!DOCTYPE html>");
		builder.append("<html lang='en'>");
		  builder.append("<head>");
		    builder.append("<meta charset='utf-8'>");
		    builder.append("<meta http-equiv='X-UA-Compatible' content='IE=edge'>");
		    builder.append("<meta name='viewport' content='width=device-width, initial-scale=1'>");
		    builder.append("<title>studentbook - registration page</title>");

		     /* Bootstrap CSS link */
		    builder.append("<link rel='stylesheet' href='http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>");
		  builder.append("</head>");
		  builder.append("<body>");
		    builder.append("<div class='container-fluid'>");
				builder.append("<div class='row'>");
					builder.append("<div class='col-lg-offset-1 col-lg-3' style='padding-top: 10%;'>");
						/* Registration form */
						builder.append("<form role='form' action='registration.s' method='post'>");
		  					builder.append("<div class='form-group'>");
		    					builder.append("<label class='sr-only' for='email'>Email:</label>");
		    					builder.append("<input type='email' class='form-control input-lg' id='email' name='email' placeholder='email' autocomplete='on' autofocus='autofocus' required='required'>");
		  					builder.append("</div>");
		  					builder.append("<div class='form-group'>");
						    	builder.append("<label class='sr-only' for='password'>Password:</label>");
						    	builder.append("<input type='password' class='form-control input-lg' id='password' name='password' placeholder='password' required='required'>");
							builder.append("</div>");
							builder.append("<div class='alert alert-danger alert-dismissible' role='alert'>");
								builder.append("<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>");
								builder.append("<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>");
								builder.append("<span class='sr-only'>Error:</span>");
								builder.append("Enter a valid email address");
							builder.append("</div>");
							builder.append("<div class='checkbox'>");
						    	builder.append("<label id='agree' class='pull-left'><input type='checkbox' name='agree'>");
						    	  builder.append("<span class='text-muted'> I agree to the <span class='text-primary'>Terms of Services</span> and <span class='text-primary'>Privacy Policy</span></span>");
						    	builder.append("</label>");
							builder.append("</div>");
							builder.append("<button id='login' type='submit' class='btn btn-success btn-lg btn-block'>");
								builder.append("<span class='glyphicon glyphicon glyphicon-log-in'></span> Register");
			  				builder.append("</button>");
						builder.append("</form>");
					builder.append("</div>");
				builder.append("</div>");
			builder.append("</div>");

		    /* Bootstrap JQuery link */
		    builder.append("<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'></script>");
		    /* Bootstrap link */
		    builder.append("<script src='http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js'></script>");
		  builder.append("</body>");
		builder.append("</html>");
		
		return builder.toString();
	}
}
