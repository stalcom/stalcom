package com.stalcom.stdbk.reg.page;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.stalcom.stdbk.dto.User;

public class AdvanceRegistrationPage extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getSession(false) == null) {
			// session does not exists
			response.sendRedirect("home.jsp");
		}
		
		PrintWriter out = response.getWriter();
		
		Map<String,User> users = (Map<String,User>) this.getServletConfig().getServletContext().getAttribute("users"); 
		
		for (Map.Entry<String,User> entry : users.entrySet()) {
			out.println(entry.getValue());
		}
		
		out.close();
	}

}
