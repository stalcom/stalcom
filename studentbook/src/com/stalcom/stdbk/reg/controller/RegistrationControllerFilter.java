package com.stalcom.stdbk.reg.controller;

import static com.stalcom.stdbk.util.InputValidator.isValidEmail;
import static com.stalcom.stdbk.util.InputValidator.isValidPassword;

import java.io.IOException;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import com.stalcom.stdbk.dto.User;

public class RegistrationControllerFilter implements Filter {
	private FilterConfig config;

	@Override
	public void destroy() {
		config = null;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		
		String email = request.getParameter("email").toLowerCase();
		String password = request.getParameter("password");
		
		if(isValidEmail(email) && isValidPassword(password)) {
			
			Map<String,User> users = (Map<String,User>) config.getServletContext().getAttribute("users");
			
			if(!users.containsKey(email)) {
				users.put(email, new User(email, password));
				chain.doFilter(request, response);
				return;
			}
		}
		
		HttpServletResponse res = (HttpServletResponse) response;
		res.sendRedirect(res.encodeRedirectURL("registration.jsp"));
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		this.config = config;
	}

}
