package com.stalcom.stdbk.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

public abstract class JDBCConnection {

	public static Connection getConnection() throws ClassNotFoundException, SQLException {
		ResourceBundle res = ResourceBundle.getBundle("db");
		
		Class.forName(res.getString("driver"));
		return DriverManager.getConnection(res.getString("url"), res.getString("user"), res.getString("password"));
	}
	
}
